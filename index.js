const express = require('express');
const morgan = require('morgan');
const mongoose = require('mongoose');
const dotenv = require('dotenv');

const app = express();

dotenv.config();

mongoose.connect(process.env.DB_URL);

const { authMiddleware } = require('./src/middleware/authMiddleware');
const { authRouter } = require('./src/authRouter');
const { usersRouter } = require('./src/usersRouter');
const { notesRouter } = require('./src/notesRouter');

app.use(express.json());
app.use(morgan('tiny'));

app.use('/api/auth', authRouter);
app.use('/api/users', authMiddleware, usersRouter);
app.use('/api/notes', authMiddleware, notesRouter);

app.listen(8080, () => {
  console.log('Server is running on Port 8080');
});

function errorHandler(err, req, res) {
  return res
    .status(err.status || 500)
    .json({ message: err.message || 'Server error' });
}

app.use(errorHandler);
