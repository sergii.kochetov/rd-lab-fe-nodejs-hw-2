const express = require('express');

const router = express.Router();

const { getUser, deleteUser, changePassword } = require('./usersService');

router.get('/me', getUser);

router.delete('/me', deleteUser);

router.patch('/me', changePassword);

module.exports = {
  usersRouter: router,
};
