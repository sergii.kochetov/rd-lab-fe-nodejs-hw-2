const bcrypt = require('bcryptjs');

const { User } = require('./models/User');

const getUser = async (req, res) => {
  res.status(200).json({
    user: req.user,
  });
};

const deleteUser = async (req, res) => {
  try {
    await User.findByIdAndDelete(req.user._id);
    return res.status(200).json({ message: 'Success' });
  } catch (err) {
    return res.status(400).json({ message: err.message });
  }
};

const changePassword = async (req, res) => {
  try {
    const { oldPassword, newPassword } = req.body;
    const { _id } = req.user;

    const { password: currentPassword } = await User.findById(_id);

    const passwordCheck = await bcrypt.compare(
      String(oldPassword),
      String(currentPassword),
    );

    if (!passwordCheck) {
      return res.status(400).json({ message: 'Old password is not correct' });
    }
    if (oldPassword === newPassword) {
      return res
        .status(400)
        .json({ message: 'Your new password matches with the old one' });
    }

    await User.findByIdAndUpdate(_id, {
      $set: { password: await bcrypt.hash(newPassword, 10) },
    });

    return res.status(200).json({ message: 'Success' });
  } catch (err) {
    return res.status(400).json({ message: err.message });
  }
};

module.exports = {
  getUser,
  deleteUser,
  changePassword,
};
