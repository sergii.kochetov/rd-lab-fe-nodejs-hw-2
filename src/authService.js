const bcrypt = require('bcryptjs');

const jwt = require('jsonwebtoken');

const { User } = require('./models/User');

const registerUser = async (req, res) => {
  try {
    const { username, password } = req.body;

    if (!username) res.status(400).json({ message: 'Username is required' });
    if (!password) {
      return res
        .status(400)
        .json({ message: 'You forgot to write down the password' });
    }

    const user = new User({
      username,
      password: await bcrypt.hash(password, 10),
    });

    await user.save();

    return res.status(200).json({ message: 'Success' });
  } catch (err) {
    return res.status(400).json({ message: err.message });
  }
};

const loginUser = async (req, res) => {
  try {
    const { username, password } = req.body;
    const user = await User.findOne({ username });

    if (
      user &&
      (await bcrypt.compare(String(password), String(user.password)))
    ) {
      const payload = {
        username: user.username,
        userId: user._id,
      };

      const jwtToken = jwt.sign(payload, process.env.S3_API);

      return res.status(200).json({
        message: 'Success',
        jwt_token: jwtToken,
      });
    }
    if (!user) {
      return res
        .status(400)
        .json({ message: 'No such user found in the database' });
    }
    return res.status(400).json({ message: 'Incorret password' });
  } catch (err) {
    return res.status(400).json({ message: err.message });
  }
};

module.exports = {
  registerUser,
  loginUser,
};
