const express = require('express');

const router = express.Router();

const {
  getNotes,
  addNote,
  getNoteById,
  updateNoteById,
  changeCompleteStatusById,
  deleteNoteById,
} = require('./notesService');

router.get('/', getNotes);

router.post('/', addNote);

router.get('/:id', getNoteById);

router.put('/:id', updateNoteById);

router.patch('/:id', changeCompleteStatusById);

router.delete('/:id', deleteNoteById);

module.exports = {
  notesRouter: router,
};
