const { Note } = require('./models/Note');

const getNotes = async (req, res) => {
  const { offset, limit } = req.query;
  const { _id: userId } = req.user;
  console.log(userId);

  try {
    const notes = await Note.find({ userId });

    return res.status(200).json({
      offset: offset || 0,
      limit: limit || 0,
      count: notes.length,
      notes: notes.slice(offset, limit || notes.length),
    });
  } catch (err) {
    return res.status(400).json({ message: err.message });
  }
};

const addNote = async (req, res) => {
  const { text } = req.body;

  try {
    const note = new Note({
      userId: req.user._id,
      text,
    });

    await note.save();

    return res.status(200).json({ message: 'Success' });
  } catch (err) {
    return res.status(400).json({ message: err.message });
  }
};

const getNoteById = async (req, res) => {
  const { id: noteId } = req.params;
  const { _id: userId } = req.user;

  if (noteId.length !== 24) {
    return res.status(400).json({ message: 'Incorrect ID format' });
  }

  try {
    const note = await Note.findOne({ _id: noteId, userId });
    if (!note) {
      return res
        .status(400)
        .json({ message: 'You don not have any notes with such ID' });
    }

    return res.status(200).json({
      note,
    });
  } catch (err) {
    return res.status(400).json({ message: err.message });
  }
};

const updateNoteById = async (req, res) => {
  const { text } = req.body;
  const { id: noteId } = req.params;
  const { _id: userId } = req.user;

  if (!text) {
    return res.status(400).json({ message: 'No text found' });
  }

  try {
    const note = await Note.findByIdAndUpdate(
      { _id: noteId, userId },
      { text },
    );

    if (!note) {
      return res.status(400).json({ message: 'No note with such ID found' });
    }

    return res.status(200).json({ message: 'Success' });
  } catch (err) {
    return res.status(400).json({ message: err.message });
  }
};

const changeCompleteStatusById = async (req, res) => {
  const { id: noteId } = req.params;
  const { _id: userId } = req.user;

  try {
    const note = await Note.findOne({ _id: noteId, userId });

    if (!note) {
      return res.status(400).json({ message: 'No note with such ID found' });
    }

    note.completed = !note.completed;

    note.save();

    return res.status(200).json({ message: 'Success' });
  } catch (err) {
    return res.status(400).json({ message: err.message });
  }
};

const deleteNoteById = async (req, res) => {
  const { id: noteId } = req.params;
  const { _id: userId } = req.user;

  try {
    const note = await Note.findByIdAndDelete({ _id: noteId, userId });

    if (!note) {
      return res.status(400).json({ message: 'No note with such ID found' });
    }

    return res.status(200).json({ message: 'Success' });
  } catch (err) {
    return res.status(400).json({ message: err.message });
  }
};

module.exports = {
  getNotes,
  addNote,
  getNoteById,
  updateNoteById,
  changeCompleteStatusById,
  deleteNoteById,
};
