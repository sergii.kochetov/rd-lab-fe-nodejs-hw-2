const mongoose = require('mongoose');

const userSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: false,
    },
    username: {
      type: String,
      required: true,
      unique: true,
    },
    password: {
      type: String,
      required: true,
    },
  },
  {
    timestamps: {
      createdAt: 'createdDate',
      updatedAt: false,
    },
  },
);

const User = mongoose.model('User', userSchema);

module.exports = {
  User,
};
