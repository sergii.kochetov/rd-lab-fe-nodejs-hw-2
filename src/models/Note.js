const mongoose = require('mongoose');

const noteSchema = new mongoose.Schema(
  {
    userId: {
      type: String,
      required: true,
    },
    completed: {
      type: Boolean,
      default: false,
      required: true,
    },
    text: {
      type: String,
      required: true,
    },
  },
  {
    timestamps: {
      createdAt: 'createdDate',
      updatedAt: false,
    },
    versionKey: false,
  },
);

const Note = mongoose.model('Note', noteSchema);

module.exports = {
  Note,
};
