const jwt = require('jsonwebtoken');
const { User } = require('../models/User');

const authMiddleware = async (req, res, next) => {
  const { authorization } = req.headers;

  try {
    if (!authorization) {
      return res
        .status(400)
        .json({ message: 'Please provide authorization header' });
    }

    const [, token] = authorization.split(' ');

    if (!token) {
      return res
        .status(400)
        .json({ message: 'Please include token to the request' });
    }

    const tokenPayload = jwt.verify(token, process.env.S3_API);

    const user = await User.findById({ _id: tokenPayload.userId });

    if (!user) {
      return res
        .status(400)
        .json({ message: 'No such user found in the database' });
    }

    req.user = {
      _id: tokenPayload.userId,
      username: tokenPayload.username,
      createdDate: user.createdDate,
    };

    return next();
  } catch (err) {
    return res.status(400).json({ message: 'Not authorized' });
  }
};

module.exports = {
  authMiddleware,
};
